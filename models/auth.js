const mongoose = require("mongoose");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const authSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, "Please Provide a Email Address"],
    unique: [true, "This is Email Address Already has a Account"],
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "Please Provide a Valid Email Address",
    ],
  },
  password: {
    type: String,
    required: [true, "Please Provide a Password"],
    minlength: [8, "Password should have 8 or more characters"],
  },
  accountType: {
    type: String,
    required: [true, "Please Select a User Type"],
  },
  userId: {
    type: String,
    required: [true, "No UserId."],
  },
  resetPasswordToken: String,
  resetPasswordTokenExpire: Date,
});

authSchema.pre("save", async function (next) {
  if (!this.isModified("password")) {
    next();
  }
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
  next();
});

authSchema.methods.getSignedToken = function () {
  return jwt.sign({ id: this._id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRESIN,
  });
};

authSchema.methods.matchPasswords = async function (password) {
  return await bcrypt.compare(password, this.password);
};

authSchema.methods.getPasswordResetToken = function () {
  const resetToken = crypto.randomBytes(20).toString("hex");

  this.resetPasswordToken = crypto
    .createHash("sha256")
    .update(resetToken)
    .digest("hex");

  this.resetPasswordTokenExpire = Date.now() + 10 * (60 * 1000);

  return resetToken;
};

module.exports = mongoose.model("auth", authSchema);
