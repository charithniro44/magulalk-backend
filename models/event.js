const mongoose = require("mongoose");

const eventSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  buisnessprofile_id: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  images: [String],
  videos: [String],
  Date: {
    type: Date,
  },
});

module.exports = mongoose.model("events", eventSchema);
