const { ObjectId } = require("bson");
const mongoose = require("mongoose");

const calenderEventSchema = new mongoose.Schema({
  user_id: {
    type: ObjectId,
    required: true,
  },
  booking: {
    booking_id: {
      type: ObjectId,
      required: true,
    },
    user: {
      user_id: {
        type: ObjectId,
        required: true,
      },
      displayname: {
        type: String,
        required: true,
      },
      profileImage: {
        type: String,
        required: true,
      },
    },
    owner: {
      owner_id: {
        type: ObjectId,
        required: true,
      },
      buisnessprofile_id: {
        type: ObjectId,
        required: true,
      },
      logo: {
        type: String,
        required: true,
      },
      title: {
        type: String,
        required: true,
      },
      category: {
        type: String,
        required: true,
      },
    },
    bookingpackage: {
      title: {
        type: String,
      },
      description: {
        type: String,
        required: true,
      },
      price: {
        type: Number,
      },
    },
    extraInfo: {
      type: String,
    },
  },
  date: {
    type: Date,
    required: true,
  },
});

module.exports = mongoose.model("calenderEvents", calenderEventSchema);
