const mongoose = require("mongoose");

const categorySchema = mongoose.Schema({
  title: {
    type: String,
    required: [true, "Please provide a category title"],
  },
  services: [String],
});

module.exports = mongoose.model("categories", categorySchema);
