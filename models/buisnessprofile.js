const { ObjectId } = require("bson");
const mongoose = require("mongoose");

const packageSchema = {
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  image: {
    type: String,
  },
  price: {
    type: Number,
    required: true,
  },
  document: {
    type: String,
  },
};

const reviewSchema = {
  user: {
    displayName: {
      type: String,
      required: true,
    },
    profileImage: {
      type: String,
      required: true,
    },
  },
  rating: {
    type: Number,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  Date: { type: Date, default: Date.now },
};

const buisnessprofileSchema = new mongoose.Schema({
  title: {
    type: String,
  },
  logo: {
    type: String,
  },
  accountCategory: {
    type: String,
  },
  description: {
    type: String,
  },
  coverPhoto: {
    type: String,
  },
  email: {
    type: String,
  },
  contactNo: {
    type: String,
  },
  address: {
    type: String,
  },
  district: {
    type: String,
  },
  rating: {
    type: Number,
  },
  isApproved: {
    type: Boolean,
    required: true,
  },
  requestDocument: {
    type: String,
  },
  packages: [packageSchema],
  reviews: [reviewSchema],
  gallery: [String],
  services: [String],
});

buisnessprofileSchema.index({ title: "text", description: "text" });

module.exports = mongoose.model("buisnessprofiles", buisnessprofileSchema);
