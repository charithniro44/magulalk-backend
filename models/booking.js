const mongoose = require("mongoose");
const User = require("../models/user");
const Buisnessprofile = require("../models/buisnessprofile");
const { ObjectId } = require("mongodb");

const BookingSchema = mongoose.Schema({
  user: {
    _id: {
      type: ObjectId,
      required: true,
    },
    profileImage: {
      type: String,
      required: true,
    },
    fullName: {
      type: String,
      required: true,
    },
    userName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    contactNo: {
      type: String,
      required: true,
    },
  },
  buisnessProfile: {
    _id: {
      type: ObjectId,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    logo: {
      type: String,
      required: true,
    },
    accountCategory: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    contactNo: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
  },
  bookingType: {
    type: String,
    required: true,
  },
  bookingPackage: {
    type: Object,
  },
  services: [String],
  extraInfo: { type: String },
  bookingDate: { type: Date, required: true },
  date: {
    type: Date,
    required: true,
  },
  isConfirmed: {
    type: String,
    required: true,
  },
  statusmessage: {
    type: String,
  },
});

module.exports = mongoose.model("bookings", BookingSchema);
