const jwt = require("jsonwebtoken");
const User = require("../models/user");
const errorResponse = require("../utils/errorResponse");

const protect = async (req, res, next) => {
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  }
  if (!token) {
    next(new errorResponse("You Don't Have Access to Private Data", 401));
  }
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const user = await User.findById(decoded.id);
    if (!user) {
      next(new errorResponse("Invalid Token.No User with that ID", 401));
    }
    req.user = user;
    next();
  } catch (err) {
    next(new errorResponse("You Don't Have Access to Private Data", 401));
  }
};

module.exports = protect;
