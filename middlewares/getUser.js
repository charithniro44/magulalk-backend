const jwt = require("jsonwebtoken");
const User = require("../models/user");
const errorResponse = require("../utils/errorResponse");

const getUser = async (req, res, next) => {
  const { token } = req.body;
  if (!token) {
    next(new errorResponse("There is no Token", 401));
  }
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const user = await User.findById(decoded.id);
    if (!user) {
      next(new errorResponse("Invalid Token.No User with that ID", 401));
    }
    req.user = user;
    next();
  } catch (err) {
    next(new errorResponse("Error.Can't get user", 401));
  }
};

module.exports = getUser;
