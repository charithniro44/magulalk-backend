const express = require("express");
const router = express.Router();
const {
  signin,
  register,
  forgetpassword,
  resetpassword,
} = require("../controllers/authcontroller");

router.post("/signin", signin);
router.post("/register", register);
router.post("/forgetpassword", forgetpassword);
router.put("/resetpassword/:resetToken", resetpassword);

module.exports = router;
