const express = require("express");
const router = express.Router();
const {
  updateProfile,
  getBuisnessProfiles,
  getBuisnessProfile,
  getBuisnessProfilesByCategory,
  searchBuisnessProfilesByQuery,
  getVendorbuisnessprofile,
  addreview,
  addpackage,
  addevent,
  deleteEvent,
} = require("../controllers/buisnessprofilecontroller");
const protect = require("../middlewares/auth");
const getUser = require("../middlewares/getUser");

router.get("/", getBuisnessProfiles);

router.put("/update-profile/:id", updateProfile);

router.get("/:id", getBuisnessProfile);

router.get("/categories/:category", getBuisnessProfilesByCategory);

router.get("/new", searchBuisnessProfilesByQuery);

router.post("/addpackage/:buisnessprofile_id", addpackage);

router.post("/addevent/:buisnessprofile_id", addevent);

router.delete("/deleteevent/:event_id", deleteEvent);

router.post("/addreview", addreview);

router.get("/mybuisnessprofile/:token", getVendorbuisnessprofile);

module.exports = router;
