const express = require("express");
const router = express.Router();
const {
  addbooking,
  getbookings,
  updatebookingstatus,
} = require("../controllers/bookingcontroller");
const protect = require("../middlewares/auth");

router.get("/:token", getbookings);
router.post("/bookingrequest", protect, addbooking);
router.put("/updatestatus/:bookingid", updatebookingstatus);

module.exports = router;
