const express = require("express");
const router = express.Router();
const { updateuserprofile } = require("../controllers/userController");
const getUser = require("../middlewares/getUser");

router.put("/updateprofile", getUser, updateuserprofile);

module.exports = router;
