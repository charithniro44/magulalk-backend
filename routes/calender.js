const express = require("express");
const router = express.Router();
const {
  createCalenderEvent,
  getCalenderEvents,
  updateCalenderEvent,
  deleteCalenderEvent,
} = require("../controllers/calendercontroller");

router.post("/create-calender-event/:token", createCalenderEvent);
router.get("/get-calender-events/:token", getCalenderEvents);
router.put("/update-calender-event/:id", updateCalenderEvent);
router.delete("/delete-calender-event/:id", deleteCalenderEvent);

module.exports = router;
