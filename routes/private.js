const express = require("express");
const router = express.Router();
const private = require("../controllers/privatecontroller");
const protect = require("../middlewares/auth");

router.get("/", protect, private);

module.exports = router;
