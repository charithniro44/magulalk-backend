const express = require("express");
const multer = require("multer");
const router = express.Router();
const {
  singlefileupload,
  multipleupload,
} = require("../controllers/fileuploadcontroller");

const filestorage = multer.diskStorage({
  destination: function (req, file, cb) {
    const filetype = file?.mimetype.split("/");
    if (filetype[0] === "video") {
      cb(null, `public/videos/`);
    } else if (filetype[0] === "image") {
      cb(null, `public/images/`);
    } else {
      cb(null, `public/documents/`);
    }
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({ storage: filestorage });

router.post("/file", upload.single("file"), singlefileupload);

router.post("/files", upload.array("file", 5), multipleupload);

module.exports = router;
