const express = require("express");
const router = express.Router();
const multer = require("multer");
const protect = require("../middlewares/auth");
const {
  addevent,
  getevent,
  getEvents,
} = require("../controllers/eventcontroller");

const filestorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `public/images/vendors/`);
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({ storage: filestorage });

router.post(
  "/create-event",
  [protect, upload.fields([{ name: "images" }, { name: "videos" }])],
  addevent
);

router.get("/get-event/:id", getevent);

router.get("/get-events/:buisnessprofile_id", getEvents);

module.exports = router;
