const express = require("express");
const router = express.Router();
const {
  addcategory,
  getcategories,
} = require("../controllers/categorycontroller");

router.post("/add-category", addcategory);

router.get("/get-categories", getcategories);

module.exports = router;
