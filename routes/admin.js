const express = require("express");
const router = express.Router();
const {
  getBuisnessprofileRequests,
  updateBuisnessprofileapprovestatus,
} = require("../controllers/admincontroller");

router.get("/requests", getBuisnessprofileRequests);
router.get("/approve/:buisnessprofile_id", updateBuisnessprofileapprovestatus);

module.exports = router;
