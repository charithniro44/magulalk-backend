const errorResponse = require("../utils/errorResponse");

const singlefileupload = (req, res, next) => {
  try {
    const file = req.file.path.substr(7);
    console.log(file);
    res.status(200).json({
      success: true,
      data: file,
    });
  } catch (err) {
    new errorResponse(`File Upload Failed.${err.message}`, 404);
  }
};

const multipleupload = (req, res, next) => {
  try {
    const files = req.files.map((file) => {
      return file.path.substr(7);
    });
    res.status(200).json({
      success: true,
      data: files,
    });
  } catch (err) {
    new errorResponse(`Files Upload Failed.${err.message}`, 404);
  }
};

module.exports = {
  singlefileupload,
  multipleupload,
};
