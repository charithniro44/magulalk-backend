const Calender = require("../models/calender");
const jwt = require("jsonwebtoken");
const errorResponse = require("../utils/errorResponse");

const createCalenderEvent = async (req, res, next) => {
  const user_id = jwt.decode(req?.params?.token, process.env.JWT_SECRET)?.id;
  const { bookingpackage, bookingdate } = req.body;
  try {
    const calenderevent = await Calender.create({
      user_id,
      bookingpackage,
      bookingdate,
    });
    res.status(200).json({
      success: true,
      data: calenderevent,
    });
  } catch (err) {
    new errorResponse(err.message, 404);
  }
};

const getCalenderEvents = async (req, res, next) => {
  const user_id = jwt.decode(req?.params?.token, process.env.JWT_SECRET)?.id;
  try {
    const calenderevents = await Calender.find({
      user_id: user_id,
    });
    res.status(200).json({
      success: true,
      data: {
        calenderEvents: calenderevents,
      },
    });
  } catch (err) {
    new errorResponse(eer.message, 404);
  }
};

const updateCalenderEvent = async (req, res, next) => {
  const id = jwt.decode(req?.params?.token, process.env.JWT_SECRET)?.id;
  const { updatedcalenderevent } = req.body;
  try {
    const calenderevents = await Calender.updateOne(
      {
        _id: id,
      },
      updatedcalenderevent
    );
    res.status(200).json({
      success: true,
      data: {
        calenderEvents: calenderevents,
      },
    });
  } catch (err) {
    new errorResponse(eer.message, 404);
  }
};

const deleteCalenderEvent = async (req, res, next) => {
  const booking_id = req?.params?.id;
  try {
    const calenderevent = await Calender.deleteOne({
      _id: booking_id,
    });
    res.status(200).json({
      success: true,
      data: "Successfully Deleted the Event",
    });
  } catch (err) {
    new errorResponse(eer.message, 404);
  }
};

module.exports = {
  createCalenderEvent,
  getCalenderEvents,
  updateCalenderEvent,
  deleteCalenderEvent,
};
