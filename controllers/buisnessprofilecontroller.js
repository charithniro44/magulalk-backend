const Buissnessprofile = require("../models/buisnessprofile");
const Event = require("../models/event");
const errorResponse = require("../utils/errorResponse");
const jwt = require("jsonwebtoken");

const updateProfile = async (req, res, next) => {
  const id = req?.params?.id;
  const data = req.body;
  try {
    const response = await Buissnessprofile.findOneAndUpdate(
      {
        _id: id,
      },
      data,
      { new: true, rawResult: true }
    );
    res.status(200).json({
      success: true,
      data: "Updated Succesfully",
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const getBuisnessProfiles = async (req, res, next) => {
  try {
    const buisnessprofiles = await Buissnessprofile.find();
    res.status(200).json({
      success: true,
      data: buisnessprofiles,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const getBuisnessProfile = async (req, res, next) => {
  const id = req?.params.id;

  try {
    const buisnessprofile = await Buissnessprofile.findOne({
      _id: id,
    });
    res.status(200).json({
      success: true,
      data: buisnessprofile,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const getBuisnessProfilesByCategory = async (req, res, next) => {
  const { category } = req.params;
  try {
    const buisnessprofiles = await Buissnessprofile.find({
      accountCategory: category,
    });
    res.status(200).json({
      success: true,
      data: buisnessprofiles,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const searchBuisnessProfilesByQuery = async (req, res, next) => {
  console.log("inside search controller");
  const { query, district, category } = req.query;

  console.log(query, district, category);

  try {
    await Buissnessprofile.createIndexes({
      title: "text",
      description: "text",
    });
    if (category === "undefined" && district === "undefined") {
      var searchResults = await Buissnessprofile.find({
        $text: { $search: query },
      });
    } else if (category === "undefined") {
      var searchResults = await Buissnessprofile.find({
        $text: { $search: query },
        district: district,
      });
    } else if (district === "undefined") {
      var searchResults = await Buissnessprofile.find({
        $text: { $search: query },
        accountCategory: category,
      });
    } else {
      var searchResults = await Buissnessprofile.find({
        $text: { $search: query },
        accountCategory: category,
        district: district,
      });
    }
    res.status(200).json({
      success: true,
      data: searchResults,
    });
  } catch (err) {
    return next(new errorResponse(err.message, 404));
  }
};

const profilerequest = async (req, res, next) => {
  const { values } = req.body;
  const email = req.user.email;
  const contactNo = req.user.contactNo;
  const isApproved = false;
  const { _id, profileImage, fullName } = req.user;
  const owner = {
    _id: _id,
    profileImage: profileImage,
    fullName: fullName,
  };
  console.log(values);

  try {
    const buisnessprofile = await Buissnessprofile.create({
      owner,
      ...values,
      email,
      contactNo,
      isApproved,
    });
    res.status(200).json({
      success: true,
      buisnessprofile: buisnessprofile,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const addpackage = async (req, res, next) => {
  const { title, description, price, image, document } = req.body;
  const buisnessprofile_id = req.params?.buisnessprofile_id;
  try {
    const buinessprofile = await Buissnessprofile.updateOne(
      { _id: buisnessprofile_id },
      {
        $push: {
          packages: {
            title,
            description,
            image,
            document,
            price,
          },
        },
      }
    );
    res.status(200).json({
      success: true,
      data: buisnessprofile_id,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const addevent = async (req, res, next) => {
  const buisnessprofile_id = req.params.buisnessprofile_id;
  const { title, description, date, videos, images } = req.body;
  try {
    const buisnessprofile = await Buissnessprofile.updateOne(
      { _id: buisnessprofile_id },
      {
        $push: {
          events: {
            title,
            description,
            date,
            images,
            videos,
          },
        },
      }
    );
    res.status(200).json({
      success: true,
      data: buisnessprofile,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const deleteEvent = async (req, res, next) => {
  const event_id = req.params.event_id;

  res.status(200).json({
    success: true,
    message: "Route is Working Successfully",
  });
};

const addreview = async (req, res, next) => {
  const { user, description, date, token, rating, buisnessprofile_id } =
    req.body;
  const decoded = jwt.verify(token, process.env.JWT_SECRET);
  user.user_id = decoded?.id;

  try {
    const review = await Buissnessprofile.updateOne(
      { _id: buisnessprofile_id },
      {
        $push: {
          reviews: {
            user,
            description,
            rating,
            date,
          },
        },
      }
    );
    res.status(200).json({
      success: true,
      data: "Review has benn Successfully Added",
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const getVendorbuisnessprofile = async (req, res, next) => {
  const decoded = jwt.decode(req.params.token, process.env.JWT_SECRET);
  try {
    const buisnessprofile = await Buissnessprofile.findOne({
      "owner.id": decoded.id,
    });
    res.status(200).json({
      success: true,
      buisnessprofile,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const getUser = async (token) => {
  const decoded = jwt.verify(token, process.env.JWT_SECRET);

  try {
    const user = await User.findOne({ _id: decoded.id });
    return user;
  } catch (err) {
    return null;
  }
};

module.exports = {
  updateProfile,
  getBuisnessProfiles,
  getBuisnessProfile,
  getBuisnessProfilesByCategory,
  searchBuisnessProfilesByQuery,
  profilerequest,
  addpackage,
  addreview,
  addevent,
  deleteEvent,
  getVendorbuisnessprofile,
};
