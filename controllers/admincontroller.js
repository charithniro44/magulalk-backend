const Buissnessprofile = require("../models/buisnessprofile");
const errorResponse = require("../utils/errorResponse");
const jwt = require("jsonwebtoken");
const User = require("../models/user");

const getBuisnessprofileRequests = async (req, res, next) => {
  try {
    const requests = await Buissnessprofile.find({ isApproved: false });
    res.status(200).json({
      success: true,
      data: requests,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const updateBuisnessprofileapprovestatus = async (req, res, next) => {
  const { buisnessprofile_id } = req.params;

  try {
    const buisnessProfile = await Buissnessprofile.findOne({
      _id: buisnessprofile_id,
    });
    buisnessProfile.isApproved = true;
    await buisnessProfile.save();
    res.status(200).json({
      success: true,
      message: "Buisness profile Approved",
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

module.exports = {
  getBuisnessprofileRequests,
  updateBuisnessprofileapprovestatus,
};
