const Booking = require("../models/booking");
const Calender = require("../models/calender");
const errorResponse = require("../utils/errorResponse");
const jwt = require("jsonwebtoken");
const { ObjectId } = require("bson");

const addbooking = async (req, res, next) => {
  const { values, activebuisnessprofile } = req.body;
  const buisnessProfile = { ...activebuisnessprofile };
  const user = { ...req.user };
  const isConfirmed = false;
  const date = new Date();

  try {
    const newbooking = await Booking.create({
      ...values,
      user,
      buisnessProfile,
      isConfirmed,
      date,
    });
    res.status(200).json({
      success: true,
      data: newbooking,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const getbookings = async (req, res, next) => {
  const token = req?.params?.token;
  const decoded = jwt.decode(token, process.env.JWT_SECRET);
  const _id = new ObjectId(decoded.id);

  try {
    const bookings = await Booking.find({
      $or: [{ "owner.owner_id": _id }, { "user.user_id": _id }],
    });
    res.status(200).json({
      success: true,
      data: bookings,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const updatebookingstatus = async (req, res, next) => {
  const bookingid = req?.params?.bookingid;
  const { newstatus } = req?.body;
  try {
    const booking = await Booking.findOne({ _id: bookingid });
    if (!booking) {
      return next(new errorResponse("Cannot Find the Booking"), 404);
    }

    booking.isConfirmed = newstatus;
    await booking.save();
    res.status(200).json({
      success: true,
      data: booking,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

module.exports = {
  addbooking,
  getbookings,
  updatebookingstatus,
};
