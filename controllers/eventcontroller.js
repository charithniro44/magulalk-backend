const Event = require("../models/event");
const errorResponse = require("../utils/errorResponse");

const addevent = async (req, res, next) => {
  const images = req.files?.images?.map((image) => {
    return image.path;
  });
  const videos = req.files?.videos?.map((video) => {
    return video.path;
  });
  const { title, description, date, buisnessprofile_id } = req.body;
  try {
    const event = await Event.create({
      title,
      description,
      buisnessprofile_id,
      images,
      videos,
      date,
    });
    res.status(200).json({
      success: true,
      data: "Event has benn Successfully Added",
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const getevent = async (req, res, next) => {
  const event_id = req?.params?.id;
  try {
    const event = await Event.findOne({ _id: event_id });
    res.status(200).json({
      success: true,
      event: event,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

const getEvents = async (req, res, next) => {
  const buisnessprofile_id = req?.params?.buisnessprofile_id;
  console.log(buisnessprofile_id);
  try {
    const events = await Event.find({
      buisnessprofile_id: buisnessprofile_id,
    });
    res.status(200).json({
      success: true,
      events: events,
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

module.exports = {
  addevent,
  getevent,
  getEvents,
};
