const Auth = require("../models/auth");
const User = require("../models/user");
const Vendor = require("../models/buisnessprofile");
const errorResponse = require("../utils/errorResponse");
const sentMail = require("../utils/sentMail");
const crypto = require("crypto");
const { time } = require("console");

const register = async (req, res, next) => {
  const { email, password, accountType } = req.body;
  var userId;
  var authUser;

  try {
    const auth = await Auth.findOne({ email });
    if (auth) {
      return next(new errorResponse("Email already Exist.", 401));
    }
    if (accountType === "Customer") {
      const user = await User.create({});
      userId = user._id;
      authUser = await Auth.create({
        email,
        password,
        accountType,
        userId,
      });
    } else if (accountType === "Vendor") {
      const isApproved = false;
      const user = await Vendor.create({ isApproved });
      userId = user._id;
      authUser = await Auth.create({
        email,
        password,
        accountType,
        userId,
      });
    } else {
      return next(new errorResponse("Account Type is Not Available.", 401));
    }
    const token = authUser.getSignedToken();
    res.status(200).json({
      success: true,
      data: {
        token: token,
        userId: userId,
      },
    });
  } catch (error) {
    return next(new errorResponse(error.message, 400));
  }
};

const signin = async (req, res, next) => {
  const { email, password } = req.body;

  try {
    const auth = await Auth.findOne({ email });
    if (!auth) {
      return next(new errorResponse("Email Address Could not be Found.", 401));
    }
    const isMatch = await auth.matchPasswords(password);
    if (!isMatch) {
      return next(new errorResponse("Password not valid.", 401));
    } else {
      const token = auth.getSignedToken();
      res.status(200).json({
        success: true,
        data: {
          token: token,
          userId: auth.userId,
        },
      });
    }
  } catch (error) {
    return next(new errorResponse(error.message, 500));
  }
};

const forgetpassword = async (req, res, next) => {
  const { email } = req.body;
  try {
    const auth = await Auth.findOne({ email });
    if (!auth) {
      return next(new errorResponse("Email Address Could not be Found", 400));
    }
    const resetToken = auth.getPasswordResetToken();
    await auth.save();
    const reserPasswordUrl = `http://localhost:3000/auth/resetpassword/${resetToken}`;
    const message = `<h1>You Have Requested to Reset Password</h1><p>You can reset your password by visiting below Url</p><a href=${reserPasswordUrl} clicktracking=off>${reserPasswordUrl}</a>`;
    try {
      sentMail({
        to: email,
        subject: "Password Reset Request",
        message: message,
      });

      res.status(200).json({
        success: true,
        data: "Mail Sent Successfully.Check Your Inbox",
      });
    } catch (err) {
      auth.resetPasswordToken = undefined;
      auth.resetPasswordTokenExpire = undefined;
      await auth.save();

      return next(new errorResponse("Could not Sent the Mail.Try Again", 404));
    }
  } catch (error) {
    return next(new errorResponse(error.message, 500));
  }
};

const resetpassword = async (req, res, next) => {
  const { password } = req.body;

  const resetPasswordToken = crypto
    .createHash("sha256")
    .update(req.params.resetToken)
    .digest("hex");

  try {
    const auth = await Auth.findOne({
      resetPasswordToken: resetPasswordToken,
      resetPasswordTokenExpire: { $gt: Date.now() },
    });
    if (!auth) {
      return next(new errorResponse("Reset Password Token not Valid"), 400);
    }
    auth.password = password;
    auth.resetPasswordToken = undefined;
    auth.resetPasswordTokenExpire = undefined;
    await auth.save();

    res.status(200).json({
      success: "true",
      data: "Password Reset Succesfully.",
    });
  } catch (error) {
    return next(new errorResponse(error.message, 500));
  }
};

module.exports = {
  signin,
  register,
  forgetpassword,
  resetpassword,
};
