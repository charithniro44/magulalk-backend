const Buisnessprofile = require("../models/buisnessprofile");
const errorResponse = require("../utils/errorResponse");

const querySearch = async (req, res, next) => {
  const { query, district, category } = req.query;

  try {
    await Buisnessprofile.createIndexes({ title: "text", description: "text" });
    if (category === "undefined" && district === "undefined") {
      var searchResults = await Buisnessprofile.find({
        $text: { $search: query },
      });
    } else if (category === "undefined") {
      var searchResults = await Buisnessprofile.find({
        $text: { $search: query },
        district: district,
      });
    } else if (district === "undefined") {
      var searchResults = await Buisnessprofile.find({
        $text: { $search: query },
        accountCategory: category,
      });
    } else {
      var searchResults = await Buisnessprofile.find({
        $text: { $search: query },
        accountCategory: category,
        district: district,
      });
    }
    res.status(200).json({
      success: true,
      results: searchResults,
    });
  } catch (err) {
    return next(new errorResponse(err.message, 404));
  }
};

module.exports = {
  querySearch,
};
