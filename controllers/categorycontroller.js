const Category = require("../models/category");
const errorResponse = require("../utils/errorResponse");

const addcategory = async (req, res, next) => {
  const { title, services } = req.body;
  try {
    const category = await Category.create({
      title,
      services,
    });
    res.status(200).json({
      success: true,
      category: category,
    });
  } catch (err) {
    return new errorResponse(err.message, 404);
  }
};

const getcategories = async (req, res, next) => {
  try {
    const categories = await Category.find();
    res.status(200).json({
      success: true,
      categories: categories,
    });
  } catch (err) {
    return new errorResponse(err.message, 404);
  }
};

module.exports = {
  addcategory,
  getcategories,
};
