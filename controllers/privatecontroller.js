const private = (req, res, next) => {
  res.status(200).json({
    success: true,
    data: "You Have Access to the Private Data",
  });
};

module.exports = private;
