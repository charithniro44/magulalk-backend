const User = require("../models/user");
const errorResponse = require("../utils/errorResponse");

const updateuserprofile = async (req, res, next) => {
  const user_id = req?.params?.user_id;
  const updates = req.body;

  try {
    const response = await Buissnessprofile.findOneAndUpdate(
      {
        _id: buisnessprofile_id,
      },
      updates,
      { new: true, rawResult: true }
    );
    res.status(200).json({
      success: true,
      data: "Updated Succesfully",
    });
  } catch (err) {
    next(new errorResponse(err.message, 404));
  }
};

module.exports = {
  updateuserprofile,
};
