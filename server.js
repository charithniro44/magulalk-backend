require("dotenv").config({ path: "./config.env" });
const express = require("express");
const path = require("path");
const app = express();
const cors = require("cors");

const auth = require("./routes/auth");
const private = require("./routes/private");
const buisnessprofile = require("./routes/buisnessprofile");
const user = require("./routes/user");
const booking = require("./routes/booking");
const calender = require("./routes/calender");
const event = require("./routes/event");
const fileupload = require("./routes/fileupload");
const category = require("./routes/category");
const admin = require("./routes/admin");

const PORT = process.env.PORT || 5000;
const conn = require("./config/dbConnection");

const errorHandler = require("./middlewares/errorHandler");

app.use(express.static("public"));
app.use(express.json());
app.use(cors());
app.use("/auth", auth);
app.use("/private", private);
app.use("/buisnessprofile", buisnessprofile);
app.use("/booking", booking);
app.use("/calender", calender);
app.use("/event", event);
app.use("/file-upload", fileupload);
app.use("/categories", category);
app.use("/admin", admin);
app.use("/user", user);
app.use(errorHandler);

if (process.env.NODE_ENV === "production") {
  app.use(express.static("./client/build"));
  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});
