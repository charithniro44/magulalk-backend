const mongoose = require("mongoose");
const url = process.env.MONGO_URI;

mongoose.connect(url, { useNewUrlParser: true });
const conn = mongoose.connection;

conn.on("open", () => {
  console.log("Connected to database succesfully");
});

conn.on("disconnected", () => {
  console.log("disconnected Successfully");
});

conn.on("error", () => {
  console.log("error In Connection");
});

module.exports = conn;
