class errorResponse extends Error {
  constructor(errormessage, statuscode) {
    super(errormessage);
    this.statuscode = statuscode;
  }
}

module.exports = errorResponse;
