const nodemailer = require("nodemailer");

const sentMail = (options) => {
  const transporter = nodemailer.createTransport({
    service: process.env.EMAIL_SERVICE,
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PASSWORD,
    },
  });

  const mailOptions = {
    from: process.env.EMAIL_FROM,
    to: options.to,
    subject: options.subject,
    html: options.message,
  };

  transporter.sendMail(mailOptions, function (err, info) {
    if (err) {
      console.log(`error ${err.message}`);
    } else {
      console.log(info);
    }
  });
};

module.exports = sentMail;
